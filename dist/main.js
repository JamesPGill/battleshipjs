/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/app.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/app.js":
/*!********************!*\
  !*** ./src/app.js ***!
  \********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("const game = __webpack_require__(/*! ./game */ \"./src/game.js\");\n\nconst app = game();\napp.setup();\n\n//# sourceURL=webpack:///./src/app.js?");

/***/ }),

/***/ "./src/game.js":
/*!*********************!*\
  !*** ./src/game.js ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("const ship = __webpack_require__(/*! ./shipFactory */ \"./src/shipFactory.js\");\nconst gameBoard = __webpack_require__(/*! ./gameboardFactory */ \"./src/gameboardFactory.js\");\nconst player = __webpack_require__(/*! ./player */ \"./src/player.js\");\nconst setupLogic = __webpack_require__(/*! ./setupLogic */ \"./src/setupLogic.js\");\nconst mainLogic = __webpack_require__(/*! ./mainLogic */ \"./src/mainLogic.js\");\n\nconst game = () => {\n    const players = [player('player'), player('enemy')];\n    const gameBoards = [gameBoard(), gameBoard()];\n\n    const makeFleet = () => {\n        const destroyer = ship('destroyer', 2);\n        const sub = ship('submarine', 3);\n        const cruiser = ship('cruiser', 3);\n        const battle = ship('battleship', 4);\n        const carrier = ship('carrier', 5);\n        return [destroyer, sub, cruiser, battle, carrier];\n    };\n    const randomPlacement = () => {\n        const x = Math.floor(Math.random() * 10);\n        const y = Math.floor(Math.random() * 10);\n        const sideWays = Math.floor(Math.random() * 2);\n        if (sideWays === 0) {\n            return {\n                coor: [x, y],\n                rotate: false\n            };\n        }\n        return {\n            coor: [x, y],\n            rotate: true\n        };\n    };\n    const initCompBoard = () => {\n        const fleet = makeFleet();\n        let shipPos;\n        let placed;\n        for (let i = 0; i < fleet.length; i++) {\n            do {\n                shipPos = randomPlacement();\n                placed = gameBoards[1].place(shipPos.coor, fleet[i], shipPos.rotate);\n            } while (!placed);\n        }\n    };\n    const placeListener = () => {\n        const setupBoard = document.getElementById('setupGrid');\n        setupBoard.onclick = (event) => {\n            const positionStr = parseInt(event.target.id.match(/[0-9]{1,}/), 10);\n            const rotated = document.querySelector('.shipRotate').checked;\n            const placeX = Math.floor(positionStr / 10);\n            const placeY = positionStr % 10;\n            const pos = [placeX, placeY];\n            console.log('TCL: setupBoard.onclick -> pos', pos)\n            if (gameBoards[0].fleet.length === 0) {\n                console.log('TCL: setupBoard.onclick -> gameBoards[0].fleet.length === 0', gameBoards[0].fleet.length === 0)\n                gameBoards[0].place(pos, ship('destroyer', 2), rotated);\n            } else if (gameBoards[0].fleet.length === 1) {\n                gameBoards[0].place(pos, ship('submarine', 3), rotated);\n            } else if (gameBoards[0].fleet.length === 2) {\n                gameBoards[0].place(pos, ship('cruiser', 3), rotated);\n            } else if (gameBoards[0].fleet.length === 3) {\n                gameBoards[0].place(pos, ship('battleship', 4), rotated);\n            } else if (gameBoards[0].fleet.length === 4) {\n                gameBoards[0].place(pos, ship('carrier', 5), rotated);\n            }\n            setupLogic.renderFleetSetup(gameBoards[0].fleet);\n            mainLogic.renderPlayerFleet(gameBoards[0].fleet);\n            setupLogic.renderSetupSelector(gameBoards);\n\n            if (gameBoards[0].fleet.length === 5) {\n                const setupContainer = document.getElementById('setupContainer');\n                setupContainer.classList.add('hide');\n                const mainContainer = document.getElementById('mainContainer');\n                mainContainer.classList.remove('hide');\n            }\n        }\n    };\n\n    const getPlayerAttack = (event) => {\n        const attackSquare = parseInt(event.target.id.match(/[0-9]{1,}/), 10);\n        const attackX = Math.floor(attackSquare / 10);\n        const attackY = attackSquare % 10;\n        return [attackX, attackY];\n    };\n    const attackListener = () => {\n        const enemyBoard = document.getElementById('enemyGrid');\n        enemyBoard.onclick = (event) => {\n            const playerAttack = getPlayerAttack(event);\n            console.log('TCL: enemyBoard.onclick -> playerAttack', playerAttack)\n            const validAttack = players[0].attack(playerAttack);\n            if (validAttack && !gameOver()) {\n                gameBoards[1].receiveAttack(validAttack);\n                mainLogic.renderMoves(players[1].name, gameBoards[1]);\n                let compMove;\n                do {\n                    compMove = players[1].randomMove();\n                } while (!compMove);\n                gameBoards[0].receiveAttack(compMove);\n                mainLogic.renderMoves(players[0].name, gameBoards[0]);\n                if (gameOver()) {\n                    const winnerName = winner();\n                    console.log(\"winner\");\n                }\n            }\n            return true;\n        };\n    };\n\n    const gameOver = () => {\n        if (gameBoards[0].sunkFleet() || gameBoards[1].sunkFleet()) {\n            return true;\n        }\n        return false;\n    };\n    const winner = () => {\n        if (gameBoards[0].sunkFleet()) {\n            return players[1].name;\n        }\n        if (gameBoards[1].sunkFleet()) {\n            return players[0].name;\n        }\n        return false;\n    };\n\n    const setup = () => {\n        setupLogic.showSetupGrid();\n        setupLogic.renderFleetSetup(gameBoards[0].fleet);\n        setupLogic.renderSetupSelector(gameBoards)\n        mainLogic.renderMainBoards();\n        placeListener();\n        initCompBoard();\n        attackListener();\n\n\n\n        // console.log(gameBoards[0].fleet);\n        // setupLogic.showSetupGrid();\n        // gameBoards[0].place([1, 1], ship('destroyer', 2), true);\n        // setupLogic.renderFleetSetup(gameBoards[0].fleet);\n        // console.log(gameBoards[0].fleet);\n\n    };\n    return {\n        setup,\n    };\n};\n\nmodule.exports = game;\n\n//# sourceURL=webpack:///./src/game.js?");

/***/ }),

/***/ "./src/gameboardFactory.js":
/*!*********************************!*\
  !*** ./src/gameboardFactory.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("const gameBoard = () => {\n    const fleet = [];\n    const hits = [];\n    const misses = [];\n\n    const shipMatch = (coor) => {\n        for (let index = 0; index < fleet.length; index += 1) {\n            const member = fleet[index];\n            for (let i = 0; i < member.ship.length; i += 1) {\n                if (member.pos.rotate && coor[0] === member.pos.x &&\n                    coor[1] === member.pos.y + i) {\n                    return [index, i];\n                }\n                if (!member.pos.rotate && coor[0] === member.pos.x + i &&\n                    coor[1] === member.pos.y) {\n                    return [index, i];\n                }\n            }\n        }\n        return false;\n    };\n\n    const place = (coor, ship, rotate = false) => {\n        if (coor[0] > 9 || coor[0] < 0 || coor[1] > 9 || coor[1] < 0) {\n            return false;\n        }\n        if ((!rotate && coor[0] + ship.length - 1 > 9) ||\n            (rotate && coor[1] + ship.length - 1 > 9)) {\n            return false;\n        }\n        const shipCoor = [];\n        const collisions = [];\n        for (let i = 0; i < ship.length; i += 1) {\n            if (rotate) {\n                shipCoor.push([coor[0], coor[1] + i]);\n            } else {\n                shipCoor.push([coor[0] + i, coor[1]]);\n            }\n            collisions.push(shipMatch(shipCoor[i]));\n        }\n        const noCollisions = collisions.every(match => match === false);\n        if (noCollisions) {\n            const newFleetMem = {};\n            newFleetMem.pos = {\n                x: coor[0],\n                y: coor[1],\n                rotate,\n            };\n            newFleetMem.ship = ship;\n            fleet.push(newFleetMem);\n            return true;\n        }\n        return false;\n    };\n\n    const receiveAttack = (coor) => {\n        const strike = shipMatch(coor);\n        if (strike === false) {\n            misses.push(coor);\n        } else {\n            const shipId = strike[0];\n            const hitBox = strike[1];\n            fleet[shipId].ship.hit(hitBox);\n            hits.push(coor);\n        }\n        return true;\n    };\n\n    const sunkFleet = () => fleet.every(member => member.ship.isSunk() === true);\n    return {\n        fleet,\n        hits,\n        misses,\n        place,\n        receiveAttack,\n        sunkFleet,\n    };\n\n};\n\nmodule.exports = gameBoard;\n\n//# sourceURL=webpack:///./src/gameboardFactory.js?");

/***/ }),

/***/ "./src/mainLogic.js":
/*!**************************!*\
  !*** ./src/mainLogic.js ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("const mainLogic = (() => {\n    const createPlayerGrid = () => {\n        const grid = document.createElement('div');\n        grid.classList.add('player');\n        grid.id = 'playerGrid';\n        for (let x = 0; x < 10; x += 1) {\n            for (let y = 0; y < 10; y += 1) {\n                const square = document.createElement('div');\n                square.classList.add('square');\n                square.id = `player-${x}${y}`;\n                grid.appendChild(square);\n            }\n        }\n        return grid;\n    };\n\n    const createEnemyGrid = () => {\n        const grid = document.createElement('div');\n        grid.classList.add('enemy');\n        grid.id = 'enemyGrid';\n        for (let x = 0; x < 10; x += 1) {\n            for (let y = 0; y < 10; y += 1) {\n                const square = document.createElement('div');\n                square.classList.add('square');\n                square.id = `enemy-${x}${y}`;\n                grid.appendChild(square);\n            }\n        }\n        return grid;\n    };\n\n    const renderMainBoards = () => {\n        const mainContainer = document.getElementById('mainContainer');\n        const playerBoard = createPlayerGrid();\n        const enemyBoard = createEnemyGrid();\n        mainContainer.appendChild(playerBoard);\n        mainContainer.appendChild(enemyBoard);\n        return true;\n    };\n\n    const renderPlayerFleet = (fleet) => {\n        const board = document.getElementById('playerGrid');\n        fleet.forEach((member) => {\n            for (let i = 0; i < member.ship.length; i += 1) {\n                let posId;\n                if (member.pos.rotate) {\n                    posId = member.pos.x * 10 + member.pos.y + i;\n                } else {\n                    posId = member.pos.x * 10 + (10 * i) + member.pos.y;\n                }\n                board.childNodes[posId].classList.add('ship');\n            }\n        });\n        return true;\n    };\n    const renderMoves = (context, gameBoard) => {\n        const board = document.getElementById(`${context}Grid`);\n        gameBoard.hits.forEach((hit) => {\n            const hitPos = hit[0] * 10 + hit[1];\n            board.childNodes[hitPos].classList.add('hit');\n        });\n        gameBoard.misses.forEach((miss) => {\n            const missPos = miss[0] * 10 + miss[1];\n            board.childNodes[missPos].classList.add('miss');\n        });\n        return true;\n    };\n\n\n\n\n    return {\n        renderMainBoards,\n        renderPlayerFleet,\n        renderMoves,\n    };\n\n})();\n\n\n\nmodule.exports = mainLogic;\n\n//# sourceURL=webpack:///./src/mainLogic.js?");

/***/ }),

/***/ "./src/player.js":
/*!***********************!*\
  !*** ./src/player.js ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("const player = (name) => {\n    const moves = [];\n    const attack = (coor) => {\n        if (moves.some(move => move[0] === coor[0] &&\n                move[1] === coor[1])) {\n            return false;\n        }\n        if (coor[0] >= 0 && coor[0] <= 9 && coor[1] >= 0 && coor[1] <= 9) {\n            moves.push(coor);\n            return coor;\n        }\n        return false;\n    };\n    const randomMove = () => {\n        const x = Math.floor(Math.random() * 10);\n        const y = Math.floor(Math.random() * 10);\n        return attack([x, y]);\n    };\n    return {\n        name,\n        attack,\n        randomMove\n    };\n};\n\nmodule.exports = player;\n\n//# sourceURL=webpack:///./src/player.js?");

/***/ }),

/***/ "./src/setupLogic.js":
/*!***************************!*\
  !*** ./src/setupLogic.js ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("const setupLogic = (() => {\n    const createSetupGrid = () => {\n        const grid = document.createElement('div');\n        grid.classList.add('setup');\n        grid.id = 'setupGrid';\n        for (let x = 0; x < 10; x += 1) {\n            for (let y = 0; y < 10; y += 1) {\n                const square = document.createElement('div');\n                square.classList.add('square');\n                square.id = `setup-${x}${y}`;\n                grid.appendChild(square);\n            }\n        }\n        return grid;\n    };\n\n    const showSetupGrid = () => {\n        const setupContainer = document.getElementById('setupContainer');\n        const grid = createSetupGrid();\n        setupContainer.appendChild(grid);\n    };\n\n    const renderFleetSetup = (fleet) => {\n        const board = document.getElementById('setupGrid');\n        fleet.forEach((member) => {\n            for (let i = 0; i < member.ship.length; i += 1) {\n                let posId;\n                if (member.pos.rotate) {\n                    posId = member.pos.x * 10 + member.pos.y + i;\n                } else {\n                    posId = member.pos.x * 10 + (10 * i) + member.pos.y;\n                }\n                board.childNodes[posId].classList.add('ship');\n            }\n        });\n        return true;\n    };\n    const renderSetupSelector = (gameBoards) => {\n        if (gameBoards[0].fleet.length === 0) {\n            document.getElementById('setupForm').innerHTML =\n                \" <legend>Click to choose where the front of the ship is (top to bottom)</legend> <fieldset> <legend > Destroyer (Length: 2) </legend>  <label> Rotate (now left to right) </label>  <input type = 'checkbox' class = 'shipRotate'> </input>  </fieldset>\";\n        } else if (gameBoards[0].fleet.length === 1) {\n            document.getElementById('setupForm').innerHTML =\n                \" <legend>Click to choose where the front of the ship is (top to bottom)</legend> <fieldset> <legend > Submarine (Length: 3) </legend>  <label> Rotate (now left to right) </label>  <input type = 'checkbox' class = 'shipRotate'> </input>  </fieldset>\";\n        } else if (gameBoards[0].fleet.length === 2) {\n            document.getElementById('setupForm').innerHTML =\n                \" <legend>Click to choose where the front of the ship is (top to bottom)</legend> <fieldset> <legend > Cruiser (Length: 3) </legend>  <label> Rotate (now left to right) </label>  <input type = 'checkbox' class = 'shipRotate'> </input>  </fieldset>\";\n        } else if (gameBoards[0].fleet.length === 3) {\n            document.getElementById('setupForm').innerHTML =\n                \" <legend>Click to choose where the front of the ship is (top to bottom)</legend> <fieldset> <legend > Battleship (Length: 4) </legend>  <label> Rotate (now left to right) </label>  <input type = 'checkbox' class = 'shipRotate'> </input>  </fieldset>\";\n        } else if (gameBoards[0].fleet.length === 4) {\n            document.getElementById('setupForm').innerHTML =\n                \" <legend>Click to choose where the front of the ship is (top to bottom)</legend> <fieldset> <legend > Carrier (Length: 5) </legend>  <label> Rotate (now left to right) </label>  <input type = 'checkbox' class = 'shipRotate'> </input>  </fieldset> \";\n        }\n    };\n\n\n\n\n    return {\n        showSetupGrid,\n        renderFleetSetup,\n        renderSetupSelector,\n    };\n\n})();\n\n\n\nmodule.exports = setupLogic;\n\n//# sourceURL=webpack:///./src/setupLogic.js?");

/***/ }),

/***/ "./src/shipFactory.js":
/*!****************************!*\
  !*** ./src/shipFactory.js ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("const ship = (name, length) => {\n    const hitboxes = new Array(length).fill(false, 0);\n\n    const hit = (pos) => {\n        if (hitboxes[pos] === false) {\n            hitboxes[pos] = true;\n            return true;\n        }\n        return false;\n    };\n    const isSunk = () => hitboxes.every(box => box === true);\n    return {\n        name,\n        length,\n        hit,\n        isSunk,\n    };\n};\n\nmodule.exports = ship;\n\n//# sourceURL=webpack:///./src/shipFactory.js?");

/***/ })

/******/ });