const ship = require('../src/shipFactory');

test('can create ship', () => {
    const shipTest = ship('1', 4);
    expect(shipTest.name).toEqual('1');
    expect(shipTest.length).toEqual(4);
});

test('ship can be hit', () => {
    const shipTest = ship('2', 3);
    expect(shipTest.hit(2)).toBeTruthy();
});

test('no double hits', () => {
    const shipTest = ship('3', 3);
    shipTest.hit(1);
    expect(shipTest.hit(1)).toBeFalsy();
});

test('ship only gets hit where it exists', () => {
    const shipTest = ship('4', 3);
    expect(shipTest.hit(5)).toBeFalsy();
});

test('ship sinnks if hit everywhere', () => {
    const shipTest = ship('5', 2);
    shipTest.hit(0);
    shipTest.hit(1);
    expect(shipTest.isSunk()).toBeTruthy();
});

test('soes not sink if not hit everywhere', () => {
    const shipTest = ship('6', 3);
    shipTest.hit(0);
    shipTest.hit(1);
    expect(shipTest.isSunk()).toBeFalsy();
});