const player = require('../src/playerFactory');

test('player has name', () => {
    const mike = player('mike');
    expect(mike.name).toEqual('mike')
});

test('can attack', () => {
    const mike = player('mike');
    expect(mike.attack([0, 0])).toBeTruthy();
});

test('can attack only once', () => {
    const mike = player('mike');
    expect(mike.attack([0, 0])).toBeTruthy();
    expect(mike.attack([0, 0])).toBeFalsy();
});

test('can randomly attack if computer', () => {
    const mike = player('mike');
    expect(mike.randomMove()).toBeTruthy();
});