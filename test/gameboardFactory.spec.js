const gameBoard = require('../src/gameboardFactory');
const ship = require('../src/shipFactory');

test('has a fleet', () => {
    const newBoard = gameBoard();
    expect(newBoard.fleet).toBeTruthy();
});

test('can place ships', () => {
    const newBoard = gameBoard();
    const testShip = ship('TestShip', 3);
    expect(newBoard.place([0, 0], testShip, false)).toBeTruthy();
    expect(newBoard.place([3, 3], testShip, false)).toBeTruthy();
});
test('can place ships sideways', () => {
    const newBoard = gameBoard();
    const testShip = ship('TestShip', 3);
    const rotate = true;
    expect(newBoard.place([8, 0], testShip, rotate)).toBeTruthy();
    expect(newBoard.place([3, 0], testShip, rotate)).toBeTruthy();
});

test('two ships cannot overlap', () => {
    const newBoard = gameBoard();
    const testShip1 = ship('TestShip', 5);
    const testShip2 = ship('TestShip', 5);
    expect(newBoard.place([0, 5], testShip1)).toBeTruthy();
    expect(newBoard.place([2, 3], testShip2, true)).toBeFalsy();
});

test('can attack a ship', () => {
    const newBoard = gameBoard();
    const testShip = ship('TestShip', 3);
    newBoard.place([0, 0], testShip);
    newBoard.receiveAttack([1, 0])
    expect(newBoard.hits).toEqual([
        [1, 0]
    ]);
});

test('can manage misses', () => {
    const newBoard = gameBoard();
    const testShip = ship('TestShip', 3);
    newBoard.place([0, 0], testShip);
    newBoard.receiveAttack([6, 0]);
    expect(newBoard.misses).toEqual([
        [6, 0]
    ]);
});

test('can sink a fleet', () => {
    const newBoard = gameBoard();
    const testShip1 = ship('TestShip', 3);
    newBoard.place([0, 0], testShip1);
    const testShip2 = ship('TestShip', 2);
    newBoard.place([0, 1], testShip2, true);
    newBoard.receiveAttack([0, 0]);
    newBoard.receiveAttack([1, 0]);
    newBoard.receiveAttack([2, 0]);
    newBoard.receiveAttack([0, 1]);
    newBoard.receiveAttack([0, 2]);
    expect(newBoard.sunkFleet()).toBeTruthy();
});