const mainLogic = (() => {
    const createPlayerGrid = () => {
        const grid = document.createElement('div');
        grid.classList.add('player');
        grid.id = 'playerGrid';
        for (let x = 0; x < 10; x += 1) {
            for (let y = 0; y < 10; y += 1) {
                const square = document.createElement('div');
                square.classList.add('square');
                square.id = `player-${x}${y}`;
                grid.appendChild(square);
            }
        }
        return grid;
    };

    const createEnemyGrid = () => {
        const grid = document.createElement('div');
        grid.classList.add('enemy');
        grid.id = 'enemyGrid';
        for (let x = 0; x < 10; x += 1) {
            for (let y = 0; y < 10; y += 1) {
                const square = document.createElement('div');
                square.classList.add('square');
                square.id = `enemy-${x}${y}`;
                grid.appendChild(square);
            }
        }
        return grid;
    };

    const renderMainBoards = () => {
        const mainContainer = document.getElementById('mainContainer');
        const playerBoard = createPlayerGrid();
        const enemyBoard = createEnemyGrid();
        mainContainer.appendChild(playerBoard);
        mainContainer.appendChild(enemyBoard);
        return true;
    };

    const renderPlayerFleet = (fleet) => {
        const board = document.getElementById('playerGrid');
        fleet.forEach((member) => {
            for (let i = 0; i < member.ship.length; i += 1) {
                let posId;
                if (member.pos.rotate) {
                    posId = member.pos.x * 10 + member.pos.y + i;
                } else {
                    posId = member.pos.x * 10 + (10 * i) + member.pos.y;
                }
                board.childNodes[posId].classList.add('ship');
            }
        });
        return true;
    };
    const renderMoves = (context, gameBoard) => {
        const board = document.getElementById(`${context}Grid`);
        gameBoard.hits.forEach((hit) => {
            const hitPos = hit[0] * 10 + hit[1];
            board.childNodes[hitPos].classList.add('hit');
        });
        gameBoard.misses.forEach((miss) => {
            const missPos = miss[0] * 10 + miss[1];
            board.childNodes[missPos].classList.add('miss');
        });
        return true;
    };




    return {
        renderMainBoards,
        renderPlayerFleet,
        renderMoves,
    };

})();



module.exports = mainLogic;