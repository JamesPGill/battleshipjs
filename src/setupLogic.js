const setupLogic = (() => {
    const createSetupGrid = () => {
        const grid = document.createElement('div');
        grid.classList.add('setup');
        grid.id = 'setupGrid';
        for (let x = 0; x < 10; x += 1) {
            for (let y = 0; y < 10; y += 1) {
                const square = document.createElement('div');
                square.classList.add('square');
                square.id = `setup-${x}${y}`;
                grid.appendChild(square);
            }
        }
        return grid;
    };

    const showSetupGrid = () => {
        const setupContainer = document.getElementById('setupContainer');
        const grid = createSetupGrid();
        setupContainer.appendChild(grid);
    };

    const renderFleetSetup = (fleet) => {
        const board = document.getElementById('setupGrid');
        fleet.forEach((member) => {
            for (let i = 0; i < member.ship.length; i += 1) {
                let posId;
                if (member.pos.rotate) {
                    posId = member.pos.x * 10 + member.pos.y + i;
                } else {
                    posId = member.pos.x * 10 + (10 * i) + member.pos.y;
                }
                board.childNodes[posId].classList.add('ship');
            }
        });
        return true;
    };
    const renderSetupSelector = (gameBoards) => {
        if (gameBoards[0].fleet.length === 0) {
            document.getElementById('setupForm').innerHTML =
                " <legend>Click to choose where the front of the ship is (top to bottom)</legend> <fieldset> <legend > Destroyer (Length: 2) </legend>  <label> Rotate (now left to right) </label>  <input type = 'checkbox' class = 'shipRotate'> </input>  </fieldset>";
        } else if (gameBoards[0].fleet.length === 1) {
            document.getElementById('setupForm').innerHTML =
                " <legend>Click to choose where the front of the ship is (top to bottom)</legend> <fieldset> <legend > Submarine (Length: 3) </legend>  <label> Rotate (now left to right) </label>  <input type = 'checkbox' class = 'shipRotate'> </input>  </fieldset>";
        } else if (gameBoards[0].fleet.length === 2) {
            document.getElementById('setupForm').innerHTML =
                " <legend>Click to choose where the front of the ship is (top to bottom)</legend> <fieldset> <legend > Cruiser (Length: 3) </legend>  <label> Rotate (now left to right) </label>  <input type = 'checkbox' class = 'shipRotate'> </input>  </fieldset>";
        } else if (gameBoards[0].fleet.length === 3) {
            document.getElementById('setupForm').innerHTML =
                " <legend>Click to choose where the front of the ship is (top to bottom)</legend> <fieldset> <legend > Battleship (Length: 4) </legend>  <label> Rotate (now left to right) </label>  <input type = 'checkbox' class = 'shipRotate'> </input>  </fieldset>";
        } else if (gameBoards[0].fleet.length === 4) {
            document.getElementById('setupForm').innerHTML =
                " <legend>Click to choose where the front of the ship is (top to bottom)</legend> <fieldset> <legend > Carrier (Length: 5) </legend>  <label> Rotate (now left to right) </label>  <input type = 'checkbox' class = 'shipRotate'> </input>  </fieldset> ";
        }
    };




    return {
        showSetupGrid,
        renderFleetSetup,
        renderSetupSelector,
    };

})();



module.exports = setupLogic;