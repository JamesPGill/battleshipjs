const ship = require('./shipFactory');
const gameBoard = require('./gameboardFactory');
const player = require('./playerFactory');
const setupLogic = require('./setupLogic');
const mainLogic = require('./mainLogic');

const game = () => {
    const players = [player('player'), player('enemy')];
    const gameBoards = [gameBoard(), gameBoard()];

    const makeFleet = () => {
        const destroyer = ship('destroyer', 2);
        const sub = ship('submarine', 3);
        const cruiser = ship('cruiser', 3);
        const battle = ship('battleship', 4);
        const carrier = ship('carrier', 5);
        return [destroyer, sub, cruiser, battle, carrier];
    };
    const randomPlacement = () => {
        const x = Math.floor(Math.random() * 10);
        const y = Math.floor(Math.random() * 10);
        const sideWays = Math.floor(Math.random() * 2);
        if (sideWays === 0) {
            return {
                coor: [x, y],
                rotate: false
            };
        }
        return {
            coor: [x, y],
            rotate: true
        };
    };
    const initCompBoard = () => {
        const fleet = makeFleet();
        let shipPos;
        let placed;
        for (let i = 0; i < fleet.length; i++) {
            do {
                shipPos = randomPlacement();
                placed = gameBoards[1].place(shipPos.coor, fleet[i], shipPos.rotate);
            } while (!placed);
        }
    };
    const placeListener = () => {
        const setupBoard = document.getElementById('setupGrid');
        setupBoard.onclick = (event) => {
            const positionStr = parseInt(event.target.id.match(/[0-9]{1,}/), 10);
            const rotated = document.querySelector('.shipRotate').checked;
            const placeX = Math.floor(positionStr / 10);
            const placeY = positionStr % 10;
            const pos = [placeX, placeY];
            console.log('TCL: setupBoard.onclick -> pos', pos)
            if (gameBoards[0].fleet.length === 0) {
                console.log('TCL: setupBoard.onclick -> gameBoards[0].fleet.length === 0', gameBoards[0].fleet.length === 0)
                gameBoards[0].place(pos, ship('destroyer', 2), rotated);
            } else if (gameBoards[0].fleet.length === 1) {
                gameBoards[0].place(pos, ship('submarine', 3), rotated);
            } else if (gameBoards[0].fleet.length === 2) {
                gameBoards[0].place(pos, ship('cruiser', 3), rotated);
            } else if (gameBoards[0].fleet.length === 3) {
                gameBoards[0].place(pos, ship('battleship', 4), rotated);
            } else if (gameBoards[0].fleet.length === 4) {
                gameBoards[0].place(pos, ship('carrier', 5), rotated);
            }
            setupLogic.renderFleetSetup(gameBoards[0].fleet);
            mainLogic.renderPlayerFleet(gameBoards[0].fleet);
            setupLogic.renderSetupSelector(gameBoards);

            if (gameBoards[0].fleet.length === 5) {
                const setupContainer = document.getElementById('setupContainer');
                setupContainer.classList.add('hide');
                const mainContainer = document.getElementById('mainContainer');
                mainContainer.classList.remove('hide');
            }
        }
    };

    const getPlayerAttack = (event) => {
        const attackSquare = parseInt(event.target.id.match(/[0-9]{1,}/), 10);
        const attackX = Math.floor(attackSquare / 10);
        const attackY = attackSquare % 10;
        return [attackX, attackY];
    };
    const attackListener = () => {
        const enemyBoard = document.getElementById('enemyGrid');
        enemyBoard.onclick = (event) => {
            const playerAttack = getPlayerAttack(event);
            console.log('TCL: enemyBoard.onclick -> playerAttack', playerAttack)
            const validAttack = players[0].attack(playerAttack);
            if (validAttack && !gameOver()) {
                gameBoards[1].receiveAttack(validAttack);
                mainLogic.renderMoves(players[1].name, gameBoards[1]);
                let compMove;
                do {
                    compMove = players[1].randomMove();
                } while (!compMove);
                gameBoards[0].receiveAttack(compMove);
                mainLogic.renderMoves(players[0].name, gameBoards[0]);
                if (gameOver()) {
                    const winnerName = winner();
                    console.log("winner");
                }
            }
            return true;
        };
    };

    const gameOver = () => {
        if (gameBoards[0].sunkFleet() || gameBoards[1].sunkFleet()) {
            return true;
        }
        return false;
    };
    const winner = () => {
        if (gameBoards[0].sunkFleet()) {
            return players[1].name;
        }
        if (gameBoards[1].sunkFleet()) {
            return players[0].name;
        }
        return false;
    };

    const setup = () => {
        setupLogic.showSetupGrid();
        setupLogic.renderFleetSetup(gameBoards[0].fleet);
        setupLogic.renderSetupSelector(gameBoards)
        mainLogic.renderMainBoards();
        placeListener();
        initCompBoard();
        attackListener();



        // console.log(gameBoards[0].fleet);
        // setupLogic.showSetupGrid();
        // gameBoards[0].place([1, 1], ship('destroyer', 2), true);
        // setupLogic.renderFleetSetup(gameBoards[0].fleet);
        // console.log(gameBoards[0].fleet);

    };
    return {
        setup,
    };
};

module.exports = game;